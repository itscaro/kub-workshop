site_name: 🍳 Un cluster k8s aux petits oignons 🧅
site_url: https://k8s.grunty.uk
site_author: TJO & MVT
site_description: Workshop documentation for k8s cluster setup
site_dir: 'public/'
docs_dir: '.'

repo_name: yodamad-workshops/2024/devoxx/kub-workshop/
repo_url: https://gitlab.com/yodamad-workshops/2024/devoxx/kub-workshop/

copyright: Copyright &copy; 2024 yodamad-workshops

exclude_docs: |
  terraform/.terraform/*
  .venv/
  media/*.png

theme:
  name: material
  features:
    - content.code.copy
    - content.code.annotate
  palette: 
    # Palette toggle for light mode
    - scheme: default
      toggle:
        icon: material/brightness-7 
        name: Switch to dark mode
    # Palette toggle for dark mode
    - scheme: slate
      toggle:
        icon: material/brightness-4
        name: Switch to light mode    

plugins:
  - same-dir
  - search
  - timetoread

markdown_extensions:
  - pymdownx.highlight:
      anchor_linenums: true
      line_spans: __span
      pygments_lang_class: true
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - attr_list
  - md_in_html      
  - pymdownx.inlinehilite
  - pymdownx.snippets
  - admonition
  - pymdownx.details

extra:
  social:
    - icon: fontawesome/brands/gitlab
      link: https://gitlab.com/linuxserver.io
    - icon: fontawesome/brands/x-twitter
      link: https://twitter.com/yodamad03
    - icon: fontawesome/brands/x-twitter
      link: https://twitter.com/farunty

# Migrate this to .pages file
nav:
  - 🏠 Home: "README.md"
  - 🧱 Terraform: "terraform/README.md"
  - 1️⃣ Init: "initial-stage/README.md"
  - 🛜 Ingress Controller: "nginx-ingress-controller/README.md"
  - 🗺️ External-DNS: "external-dns/README.md"
  - 🪪 Cert-manager: "cert-manager/README.md"
  - 🔑 External-secrets: "external-secrets/README.md"
  - 🛂 Kyverno: "kyverno/README.md"
  - 🌱 Kube-downscaler: "kube-downscaler/README.md"
  - 👨🏻‍🍳 Recette: "recipe.md"
  - 🎪 Demos:
    - 🦊 GitLab env: "gitlab/README.md"
    - 🤖 Flux: "flux/README.md"